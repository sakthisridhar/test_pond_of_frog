<!DOCTYPE html>
<html>
<head>
<link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet"/>
<title><?php echo $title; ?></title>
</head>
<body>
	<div id="container">
		<h1><?php echo $title; ?></h1>
		<div id="body">
			<?php echo $main_content; ?>
		</div>
	</div>
	
</body>

</html> 