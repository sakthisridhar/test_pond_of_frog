<?php
class Manage_frog extends CI_Controller {
	public function __construct() {	
		parent::__construct();
		//load model and library
		$this->load->model('common_model');
		$this->load->library('form_validation');
	}
	//Index 
	public function index(){
		$data['frogs']=$this->common_model->get_records('frogs');
		$data['title']	="Manage Frogs";
		$data['main_content']=$this->load->view('manage_frogs',$data,true);
		$this->load->view('template',$data);
	}
	
	//Add 
	public function add(){
		$data['title']	="Manage Frogs - Add";
		$data['flag']="0";
		$data['main_content']=$this->load->view('manage_frogs_add',$data,true);
		$this->load->view('template',$data);	
	}
	
	//Save & Update
	public function save(){
		$this->form_validation->set_rules('frog_name', 'Name', 'required');
		$this->form_validation->set_rules('frog_mating', 'Mating', 'required');
		$this->form_validation->set_rules('frog_gender', 'Gender', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE){
			if($this->input->post('flag')==1){
				$data['frogs']=$this->common_model->getRow('frogs',array('*'),array('id'=>$this->input->post('id')));
				$data['flag']="1";
				$data['title']	="Manage Frogs - Edit";
				$data['main_content']=$this->load->view('manage_frogs_edit',$data,true);
			}else{
				$data['flag']="0";
				$data['title']	="Manage Frogs - Add";
				$data['main_content']=$this->load->view('manage_frogs_add',$data,true);
			}
			$this->load->view('template',$data);	
		}else{
			//initialize post value to variable
			$name=$this->input->post('frog_name');
			$gender=$this->input->post('frog_gender');
			$mating=$this->input->post('frog_mating');
			
			$data=array(
				'name'=>$name,
				'gender'=>$gender,
				'mating'=>$mating,
			);
			//if flag is 1 its goes to edit otherwise save
			if($this->input->post('flag')==1){
				
				$where=array('id'=>$this->input->post('id'));
				$this->common_model->update('frogs',$data,$where);
				$this->session->set_flashdata('sucess', '<div class="sucess">Record Updated Successfully</div>');
				
			}else{
				
				$last_id=$this->common_model->insert('frogs',$data);
				if($last_id){
					$this->session->set_flashdata('sucess', '<div class="sucess">Record Saved Successfully</div>');
				}
				
			}
			redirect(base_url());	
		}
	}
	
	//Edit 
	public function edit($id){
		$data['frogs']=$this->common_model->getRow('frogs',array('*'),array('id'=>$id));
		$data['flag']="1";
		$data['title']	="Manage Frogs - Edit";
		$data['main_content']=$this->load->view('manage_frogs_edit',$data,true);
		$this->load->view('template',$data);	
	}
	
	//Delete
	public function delete($id){
		$del=$this->common_model->delete('frogs',array('id'=>$id));
		$this->session->set_flashdata('sucess', '<div class="sucess">Record Deleted Successfully</div>');
		redirect(base_url());			
	}
}