<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_Model extends CI_Model{
	
    /*** Update table without set ***/
    public function update($table, $dataArray, $whereArray){
        if (!empty($whereArray))
            $this->db->where($whereArray);
        $this->db->update($table, $dataArray);
        return $this->db->affected_rows();
    }
	
	/***    Delete a record from table ***/
    public function delete($table, $where){
        if (is_array($where)) {
            $this->db->where($where);
        } else {
            $this->db->where('(' . $where . ')');
        }
        $this->db->delete($table);
        return TRUE;
    }
	
    /***    Function to insert and get last insert id in return ***/
    public function insert($table, $insert_array){
        $return_var = false;
        $this->db->trans_start();
        $insert_response = $this->db->insert($table, $insert_array);
        if ($insert_response) {
            $insert_id  = $this->db->insert_id();
            $return_var = $insert_id;
        }
        $this->db->trans_complete();
        return $return_var;
    }

    /***    Function to get table rows with conditions and limit ***/
    public function get_records($table, $fields = array('*'), $conditions = array(), $return = 'result_array', $sort_field = null, $order_by = 'desc', $group_by = array(), $limit_start = null, $limit_end = null, $remove_apos = true){
        $this->db->select($fields, $remove_apos);
        $this->db->from($table);
        if (!empty($conditions)) {
            foreach ($conditions as $key => $cond) {
                if (!isset($cond[3]))
                    $cond[3] = "where";
                if ($cond[0]) {
                    if (isset($cond[4]))
                        $this->db->$cond[3]($cond[1], $cond[2], $cond[4]);
                    else
                        $this->db->$cond[3]($cond[1], $cond[2]);
                } else
                    $this->db->$cond[3]($cond[1]);
            }
        }
        if (!$sort_field)
            $this->db->order_by('id', $order_by);
        else
            $this->db->order_by($sort_field, $order_by);
        if ($group_by)
            $this->db->group_by($group_by);
        if ($limit_start != '' || $limit_end != '')
            $this->db->limit($limit_start, $limit_end);
        $query  = $this->db->get();
        $return = ($return) ? $return : 'result_array';
        return $query->$return();
    }

   /***    Function to get record by id  ***/
    public function getRow($table, $fields = array('*'), $where){
        $this->db->select($fields);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row_array();
    }
}